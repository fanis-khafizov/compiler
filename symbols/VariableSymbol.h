#pragma once

#include "BaseSymbol.h"
#include <string>
#include <llvm/IR/Value.h>

struct VariableSymbol: public BaseSymbol {
  llvm::Value* value_ = nullptr;
  VariableSymbol();
  VariableSymbol(std::string name);
  VariableSymbol(std::string name, std::string type);
  ~VariableSymbol() = default;
};
