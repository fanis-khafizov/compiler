#pragma once

#include <string>
#include <llvm/IR/Value.h>

struct BaseSymbol {
  std::string name_;
  std::string base_type_;
  std::string type_;
  virtual ~BaseSymbol() = default;
};
