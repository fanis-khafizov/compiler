#pragma once

#include "BaseSymbol.h"
#include "VariableSymbol.h"
#include <string>
#include <vector>

struct FunctionSymbol: public BaseSymbol {
  std::vector<VariableSymbol> args;
  FunctionSymbol();
  FunctionSymbol(std::string name);
  ~FunctionSymbol() = default;
};
