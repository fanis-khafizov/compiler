#include "VariableSymbol.h"
#include <string>

VariableSymbol::VariableSymbol() {
  base_type_ = "var";
}

VariableSymbol::VariableSymbol(std::string name) : VariableSymbol() {
  name_ = name;
}

VariableSymbol::VariableSymbol(std::string name, std::string type) : VariableSymbol(name) {
  type_ = type;
}

