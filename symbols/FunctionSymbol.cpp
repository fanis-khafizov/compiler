#include "FunctionSymbol.h"

FunctionSymbol::FunctionSymbol() {
  base_type_ = "func";
  type_ = "void";
}

FunctionSymbol::FunctionSymbol(std::string name) : FunctionSymbol() {
  name_ = name;
}
