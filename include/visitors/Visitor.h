#pragma once

#include "forward_decl.h"

class Visitor {
public:
    virtual void Visit(NumberExpression* expression) = 0;
    virtual void Visit(IdentExpression* expression) = 0;
    virtual void Visit(NestedExpression* expression) = 0;
    virtual void Visit(EqualExpression* expression) = 0;
    virtual void Visit(NotEqualExpression* expression) = 0;
    virtual void Visit(LessExpression* expression) = 0;
    virtual void Visit(LessEqualExpression* expression) = 0;
    virtual void Visit(GreaterExpression* expression) = 0;
    virtual void Visit(GreaterEqualExpression* expression) = 0;
    virtual void Visit(AddExpression* expression) = 0;
    virtual void Visit(SubExpression* expression) = 0;
    virtual void Visit(MulExpression* expression) = 0;
    virtual void Visit(DivExpression* expression) = 0;
    virtual void Visit(PrintStatement* statement) = 0;
    virtual void Visit(VarDecl* var_decl) = 0;
    virtual void Visit(IfElseStatement* statement) = 0;
    virtual void Visit(Assignment* assignment) = 0;
    virtual void Visit(StatementList* statement_list) = 0;
    virtual void Visit(Program* program) = 0;
};
