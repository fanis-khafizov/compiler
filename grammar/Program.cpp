#include "Program.h"

Program::Program(StatementList* statement_list) : statement_list_(statement_list) {}

void Program::Accept(Visitor* visitor) {
    visitor->Visit(this);
}