%skeleton "lalr1.cc"
%require "3.5"

%defines
%define api.token.constructor
%define api.value.type variant
%define parse.assert

%code requires {
    #include <string>
    class Scanner;
    class Driver;
    
    #include "visitors/forward_decl.h"
}

// %param { Driver &drv }

%define parse.trace
%define parse.error verbose

%code {
    #include "driver.hh"
    #include "location.hh"

    #include "visitors/elements.h"
    #include "Program.h"

    #include <iostream>

    static yy::parser::symbol_type yylex(Scanner &scanner, Driver& driver) {
        return scanner.ScanToken();
    }
}

%lex-param { Scanner &scanner }
%lex-param { Driver &driver }
%parse-param { Scanner &scanner }
%parse-param { Driver &driver }

%locations

%define api.token.prefix {TOK_}

%token
    END 0 "end of file"
    EQ "=="
    NEQ "!="
    LT "<"
    LEQ "<="
    GT ">"
    GEQ ">="
    ADD "+"
    SUB "-"
    MUL "*"
    DIV "/"
    ASSIGN "="
    LPAREN "("
    RPAREN ")"
    SEMICOLON ";"
    COLON ":"
    RIGHTSCOPE "{"
    LEFTSCOPE "}"
    PRINT "print"
    DECLARE "declare"
    INT "int"
    IF "if"
    ELSE "else"
    FUNC "func"
;

%token <std::string> IDENTIFIER "identifier"
%token <int> NUMBER "number"

%nterm <Expression*> exp
%nterm <Statement*> statement
%nterm <StatementList*> statements
%nterm <Program*> unit
%%
%start unit;

%left "+" "-" "*" "/";

unit: "func" "identifier" "(" ")" "{" statements "}" { $$ = new Program($6); driver.program = $$; };

statements:
    %empty { $$ = new StatementList(); }
    | statements statement { $1->AddStatement($2); $$ = $1; }
    ;

statement:
    "identifier" "=" exp ";" { $$ = new Assignment($1, $3); }
    | "print" "(" exp ")" ";" { $$ = new PrintStatement($3); }
    | "declare" "identifier" ":" "int" ";" { $$ = new VarDecl($2); }
    | "if" "(" exp ")" "{" statements "}" "else" "{" statements "}" { $$ = new IfElseStatement($3, $6, $10); }
    ;

exp:
    "number" { $$ = new NumberExpression($1); }
    | "identifier" { $$ = new IdentExpression($1); }
    | "(" exp ")" { $$ = new NestedExpression($2); }
    | exp "==" exp { $$ = new EqualExpression($1, $3); }
    | exp "!=" exp { $$ = new NotEqualExpression($1, $3); }
    | exp "<" exp { $$ = new LessExpression($1, $3); }
    | exp "<=" exp { $$ = new LessEqualExpression($1, $3); }
    | exp ">" exp { $$ = new GreaterExpression($1, $3); }
    | exp ">=" exp { $$ = new GreaterEqualExpression($1, $3); }
    | exp "+" exp { $$ = new AddExpression($1, $3); }
    | exp "-" exp { $$ = new SubExpression($1, $3); }
    | exp "*" exp { $$ = new MulExpression($1, $3); }
    | exp "/" exp { $$ = new DivExpression($1, $3); }
    ;

%%
void
yy::parser::error(const location_type& l, const std::string& m)
{
  std::cerr << l << ": " << m << '\n';
}
