#pragma once

#include <visitors/Visitor.h>
#include "statements/StatementList.h"
//#include "expressions/Expression.h"
//#include <functions/FunctionList.h>

class Program {
public:
    Program(StatementList* statement_list);
    void Accept(Visitor*);
    StatementList* statement_list_;
};
