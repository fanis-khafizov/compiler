#pragma once

#include <expressions/Expression.h>
#include "StatementList.h"

class IfElseStatement: public Statement {
public:
    IfElseStatement(Expression* expression, StatementList* if_statements, StatementList* else_statements);
    void Accept(Visitor* visitor);

    Expression* expression_;
    StatementList* if_statements_;
    StatementList* else_statements_;
};
