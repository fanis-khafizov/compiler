#include "IfElseStatement.h"

IfElseStatement::IfElseStatement(
    Expression* expression,
    StatementList* if_statements,
    StatementList* else_statements
) : expression_(expression),
    if_statements_(if_statements),
    else_statements_(else_statements) {}

void IfElseStatement::Accept(Visitor* visitor) {
    visitor->Visit(this);
}