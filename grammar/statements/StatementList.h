#pragma once

#include "Statement.h"
#include <base_elements/BaseElement.h>
#include <vector>

class StatementList : public Statement {
    public:
    void AddStatement(Statement*);
    void Accept(Visitor*);

    std::vector<Statement*> statements_;
};
