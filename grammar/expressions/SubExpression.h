#pragma once

#include "Expression.h"

class SubExpression: public Expression {
public:
    SubExpression(Expression* left_expression, Expression* right_expression);
    void Accept(Visitor* visitor);

    Expression* left_expression_;
    Expression* right_expression_;
};