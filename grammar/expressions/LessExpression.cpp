#include "LessExpression.h"

LessExpression::LessExpression(Expression* left_expression, Expression* right_expression) :
    left_expression_(left_expression),
    right_expression_(right_expression) {}

void LessExpression::Accept(Visitor* visitor) {
    visitor->Visit(this);
}