#pragma once

#include "Expression.h"

class NotEqualExpression: public Expression {
public:
    NotEqualExpression(Expression* left_expression, Expression* right_expression);
    void Accept(Visitor* visitor) override;

    Expression* left_expression_;
    Expression* right_expression_;
};