#pragma once

#include "Expression.h"

class AddExpression: public Expression {
public:
    AddExpression(Expression* left_expression, Expression* right_expression);
    void Accept(Visitor* visitor);

    Expression* left_expression_;
    Expression* right_expression_;
};