#pragma once

#include "Expression.h"

class GreaterEqualExpression: public Expression {
public:
    GreaterEqualExpression(Expression* left_expression, Expression* right_expression);
    void Accept(Visitor* visitor) override;

    Expression* left_expression_;
    Expression* right_expression_;
};