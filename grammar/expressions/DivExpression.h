#pragma once

#include "Expression.h"

class DivExpression: public Expression {
public:
    DivExpression(Expression* left_expression, Expression* right_expression);
    void Accept(Visitor* visitor);

    Expression* left_expression_;
    Expression* right_expression_;
};