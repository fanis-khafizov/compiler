#pragma once

#include "Expression.h"

class NestedExpression: public Expression {
public:
    NestedExpression(Expression* expression);
    void Accept(Visitor* visitor) override;

    Expression* expression_;
};