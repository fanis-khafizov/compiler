#pragma once

#include "Expression.h"

class GreaterExpression: public Expression {
public:
    GreaterExpression(Expression* left_expression, Expression* right_expression);
    void Accept(Visitor* visitor) override;

    Expression* left_expression_;
    Expression* right_expression_;
};