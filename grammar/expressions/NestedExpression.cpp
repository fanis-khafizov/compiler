#include "NestedExpression.h"

NestedExpression::NestedExpression(Expression* expression) : expression_(expression) {}

void NestedExpression::Accept(Visitor* visitor) {
    visitor->Visit(this);
}