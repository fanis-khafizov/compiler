#include "DivExpression.h"

DivExpression::DivExpression(Expression* left_expression, Expression* right_expression) :
    left_expression_(left_expression),
    right_expression_(right_expression) {}

void DivExpression::Accept(Visitor* visitor) {
    visitor->Visit(this);
}