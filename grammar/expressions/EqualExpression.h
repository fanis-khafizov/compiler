#pragma once

#include "Expression.h"

class EqualExpression: public Expression {
public:
    EqualExpression(Expression* left_expression, Expression* right_expression);
    void Accept(Visitor* visitor) override;

    Expression* left_expression_;
    Expression* right_expression_;
};