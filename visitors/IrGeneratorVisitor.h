#pragma once

#include "TemplateVisitor.h"
#include "scopes/BaseScope.h"
#include <driver.hh>

#include <llvm/IR/Value.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/Module.h>

#include <memory>
#include <unordered_map>

class IrGeneratorVisitor: public TemplateVisitor<llvm::Value*> {
public:
  IrGeneratorVisitor(BaseScope* symbol_table);
  void Visit(NumberExpression* expression) override;
  void Visit(IdentExpression* expression) override;
  void Visit(NestedExpression* expression) override;
  void Visit(EqualExpression* expression) override;
  void Visit(NotEqualExpression* expression) override;
  void Visit(LessExpression* expression) override;
  void Visit(LessEqualExpression* expression) override;
  void Visit(GreaterExpression* expression) override;
  void Visit(GreaterEqualExpression* expression) override;
  void Visit(AddExpression* expression) override;
  void Visit(SubExpression* expression) override;
  void Visit(MulExpression* expression) override;
  void Visit(DivExpression* expression) override;
  void Visit(PrintStatement* statement) override;
  void Visit(VarDecl* var_decl) override;
  void Visit(IfElseStatement* statement) override;
  void Visit(Assignment* assignment) override;
  void Visit(StatementList* statement_list) override;
  void Visit(Program* program) override;
  llvm::Module* GetModule();

private:
  llvm::LLVMContext context_;
  llvm::IRBuilder<> builder_;
  llvm::Module module_;
  BaseScope* symbol_table_;
  llvm::Function* printfFunction;
};
