#include "IrGeneratorVisitor.h"
#include <llvm/ADT/APInt.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/DerivedTypes.h>
#include <llvm/IR/Function.h>
#include "elements.h"
#include "scopes/BaseScope.h"
#include "symbols/VariableSymbol.h"
#include <iostream>
#include <string>

IrGeneratorVisitor::IrGeneratorVisitor(BaseScope* symbol_table) :
    builder_(context_),
    module_("file", context_),
    symbol_table_(symbol_table)
{
  
  auto pointer_type = builder_.getInt8PtrTy();
  auto printfType = llvm::FunctionType::get(
      builder_.getInt32Ty(),
      {pointer_type},
      true
    );
  printfFunction = llvm::Function::Create(
      printfType,
      llvm::Function::ExternalLinkage,
      "printf",
      module_
    );
}

void IrGeneratorVisitor::Visit(NumberExpression* expression) {
  auto alloca = builder_.CreateAlloca(builder_.getInt32Ty());

  llvm::Value* value = builder_.getInt32(expression->value_);
  builder_.CreateStore(value, alloca);
  SetValue(alloca);
}

void IrGeneratorVisitor::Visit(IdentExpression* expression) {
  auto symbol = symbol_table_->GetElement(expression->ident_);
  auto var_symbol = dynamic_cast<VariableSymbol*>(symbol);
  SetValue(var_symbol->value_);
}

void IrGeneratorVisitor::Visit(NestedExpression* expression) {
  expression->expression_->Accept(this);
}

void IrGeneratorVisitor::Visit(EqualExpression* expression) {
  auto left = Accept(expression->left_expression_);
  auto right = Accept(expression->right_expression_);

  auto int_ty = builder_.getInt32Ty();
  auto alloca = builder_.CreateAlloca(int_ty);
  auto bool_value = builder_.CreateICmpEQ(
      builder_.CreateLoad(int_ty, left),
      builder_.CreateLoad(int_ty, right)
    );
  auto value = builder_.CreateIntCast(
      bool_value,
      builder_.getInt32Ty(),
      false
    );
  builder_.CreateStore(value, alloca);
  SetValue(alloca);
}

void IrGeneratorVisitor::Visit(NotEqualExpression* expression) {
  auto left = Accept(expression->left_expression_);
  auto right = Accept(expression->right_expression_);

  auto int_ty = builder_.getInt32Ty();
  auto alloca = builder_.CreateAlloca(int_ty);
  auto bool_value = builder_.CreateICmpNE(
      builder_.CreateLoad(int_ty, left),
      builder_.CreateLoad(int_ty, right)
    );
  auto value = builder_.CreateIntCast(
      bool_value,
      builder_.getInt32Ty(),
      false
    );
  builder_.CreateStore(value, alloca);
  SetValue(alloca);
}

void IrGeneratorVisitor::Visit(LessExpression* expression) {
  auto left = Accept(expression->left_expression_);
  auto right = Accept(expression->right_expression_);

  auto int_ty = builder_.getInt32Ty();
  auto alloca = builder_.CreateAlloca(int_ty);
  auto bool_value = builder_.CreateICmpSGE(
      builder_.CreateLoad(int_ty, right),
      builder_.CreateLoad(int_ty, left)
    );
  auto value = builder_.CreateIntCast(
      bool_value,
      builder_.getInt32Ty(),
      false
    );
  builder_.CreateStore(value, alloca);
  SetValue(alloca);
}

void IrGeneratorVisitor::Visit(LessEqualExpression* expression) {
  auto left = Accept(expression->left_expression_);
  auto right = Accept(expression->right_expression_);

  auto int_ty = builder_.getInt32Ty();
  auto alloca = builder_.CreateAlloca(int_ty);
  auto bool_value = builder_.CreateICmpSLE(
      builder_.CreateLoad(int_ty, left),
      builder_.CreateLoad(int_ty, right)
    );
  auto value = builder_.CreateIntCast(
      bool_value,
      builder_.getInt32Ty(),
      false
    );
  builder_.CreateStore(value, alloca);
  SetValue(alloca);
}

void IrGeneratorVisitor::Visit(GreaterExpression* expression) {
  auto left = Accept(expression->left_expression_);
  auto right = Accept(expression->right_expression_);

  auto int_ty = builder_.getInt32Ty();
  auto alloca = builder_.CreateAlloca(int_ty);
  auto bool_value = builder_.CreateICmpSLE(
      builder_.CreateLoad(int_ty, right),
      builder_.CreateLoad(int_ty, left)
    );
  auto value = builder_.CreateIntCast(
      bool_value,
      builder_.getInt32Ty(),
      false
    );
  builder_.CreateStore(value, alloca);
  SetValue(alloca);
}

void IrGeneratorVisitor::Visit(GreaterEqualExpression* expression) {
  auto left = Accept(expression->left_expression_);
  auto right = Accept(expression->right_expression_);

  auto int_ty = builder_.getInt32Ty();
  auto alloca = builder_.CreateAlloca(int_ty);
  auto bool_value = builder_.CreateICmpSGE(
      builder_.CreateLoad(int_ty, left),
      builder_.CreateLoad(int_ty, right)
    );
  auto value = builder_.CreateIntCast(
      bool_value,
      builder_.getInt32Ty(),
      false
    );
  builder_.CreateStore(value, alloca);
  SetValue(alloca);
}

void IrGeneratorVisitor::Visit(AddExpression* expression) {
  auto left = Accept(expression->left_expression_);
  auto right = Accept(expression->right_expression_);

  auto int_ty = builder_.getInt32Ty();
  auto alloca = builder_.CreateAlloca(int_ty);
  auto value = builder_.CreateAdd(
      builder_.CreateLoad(int_ty, left),
      builder_.CreateLoad(int_ty, right)
    );  
  builder_.CreateStore(value, alloca);

  SetValue(alloca);
}

void IrGeneratorVisitor::Visit(SubExpression* expression) {
  auto left = Accept(expression->left_expression_);
  auto right = Accept(expression->right_expression_);

  auto int_ty = builder_.getInt32Ty();
  auto alloca = builder_.CreateAlloca(int_ty);
  auto value = builder_.CreateSub(
      builder_.CreateLoad(int_ty, left),
      builder_.CreateLoad(int_ty, right)
    );  
  builder_.CreateStore(value, alloca);

  SetValue(alloca);
}

void IrGeneratorVisitor::Visit(MulExpression* expression) {
  auto left = Accept(expression->left_expression_);
  auto right = Accept(expression->right_expression_);

  auto int_ty = builder_.getInt32Ty();
  auto alloca = builder_.CreateAlloca(int_ty);
  auto value = builder_.CreateMul(
      builder_.CreateLoad(int_ty, left),
      builder_.CreateLoad(int_ty, right)
    );  
  builder_.CreateStore(value, alloca);

  SetValue(alloca);
}
void IrGeneratorVisitor::Visit(DivExpression* expression) {
  auto left = Accept(expression->left_expression_);
  auto right = Accept(expression->right_expression_);

  auto int_ty = builder_.getInt32Ty();
  auto alloca = builder_.CreateAlloca(int_ty);
  auto value = builder_.CreateSDiv(
      builder_.CreateLoad(int_ty, left),
      builder_.CreateLoad(int_ty, right)
    );  
  builder_.CreateStore(value, alloca);

  SetValue(alloca);
}

void IrGeneratorVisitor::Visit(PrintStatement* statement) {
  std::string string = "%d\n";
  auto fmt = llvm::ConstantDataArray::getString(context_, string);

  auto alloca = builder_.CreateAlloca(fmt->getType());
  builder_.CreateStore(fmt, alloca);

  auto formatted_string = builder_.CreateBitCast(alloca, builder_.getInt8PtrTy());

  auto pointer = Accept(statement->expression_);

  auto value = builder_.CreateLoad(builder_.getInt32Ty(), pointer);

  
  SetValue(builder_.CreateCall(printfFunction, {formatted_string, value}, "printCall"));
}

void IrGeneratorVisitor::Visit(VarDecl* var_decl) {
  auto function = builder_.GetInsertBlock()->getParent();
  llvm::BasicBlock& block = function->getEntryBlock();

  llvm::IRBuilder<> tmp(&block, block.begin());

  auto variable = tmp.CreateAlloca(builder_.getInt32Ty(), nullptr, var_decl->variable_);
  
  auto symbol = symbol_table_->GetElement(var_decl->variable_);
  auto var_symbol = dynamic_cast<VariableSymbol*>(symbol);
  var_symbol->value_ = variable;
}

void IrGeneratorVisitor::Visit(IfElseStatement* statement) {
  auto function = builder_.GetInsertBlock()->getParent();
  auto int_ty = builder_.getInt32Ty();

  auto true_entry = llvm::BasicBlock::Create(context_, "btrue", function);
  auto false_entry = llvm::BasicBlock::Create(context_, "bfalse", function);
  auto end_entry = llvm::BasicBlock::Create(context_, "bend", function);

  auto condition = Accept(statement->expression_);

  auto zero = builder_.CreateAlloca(int_ty);
  builder_.CreateStore(
      llvm::ConstantInt::get(int_ty, llvm::APInt(32, 0)),
      zero
    );

  builder_.CreateCondBr(
      builder_.CreateICmpNE(
        builder_.CreateLoad(int_ty, condition),
        builder_.CreateLoad(int_ty, zero)
      ),
      true_entry,
      false_entry
    );
  
  builder_.SetInsertPoint(true_entry);
  symbol_table_ = symbol_table_->GetScope("if" + std::to_string(symbol_table_->counter));
  statement->if_statements_->Accept(this);
  symbol_table_ = symbol_table_->GetParent();
  builder_.CreateBr(end_entry);

  builder_.SetInsertPoint(false_entry);
  symbol_table_ = symbol_table_->GetScope("else" + std::to_string(symbol_table_->counter));
  statement->else_statements_->Accept(this);
  symbol_table_ = symbol_table_->GetParent();
  builder_.CreateBr(end_entry);

  builder_.SetInsertPoint(end_entry);
  ++symbol_table_->counter;
}

void IrGeneratorVisitor::Visit(Assignment* assignment) {
  auto pointer = Accept(assignment->expression_);
  auto value = builder_.CreateLoad(builder_.getInt32Ty(), pointer);
  auto symbol = symbol_table_->GetElement(assignment->variable_);
  auto var_symbol = dynamic_cast<VariableSymbol*>(symbol);
  auto variable = var_symbol->value_;
  builder_.CreateStore(value, variable);
}

void IrGeneratorVisitor::Visit(StatementList* statement_list) {
  for (auto& statement: statement_list->statements_) {
    statement->Accept(this);
  }
}

void IrGeneratorVisitor::Visit(Program* program) {
  auto function_type = llvm::FunctionType::get(llvm::Type::getVoidTy(context_), false);

  auto function_llvm = llvm::Function::Create(
      function_type,
      llvm::Function::ExternalLinkage,
      "main",
      module_
  );

  auto entry = llvm::BasicBlock::Create(context_, "entry", function_llvm);

  builder_.SetInsertPoint(entry);
  
  symbol_table_ = symbol_table_->GetScope("main");

  program->statement_list_->Accept(this);

  builder_.CreateRetVoid();

  symbol_table_ = symbol_table_->GetParent();
  symbol_table_->Reset();
}

llvm::Module* IrGeneratorVisitor::GetModule() {
  return &module_;
}
