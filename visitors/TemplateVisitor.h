#pragma once

#include <visitors/Visitor.h>

template <typename T>
class TemplateVisitor: public Visitor {
public:
    T Accept(BaseElement* element);
    void SetValue(T value);
protected:
    T tos_value_;
};
