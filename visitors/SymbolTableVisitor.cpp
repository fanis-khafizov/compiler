#include "SymbolTableVisitor.h"
#include <string>
#include "elements.h"
#include "scopes/BaseScope.h"
#include "symbols/VariableSymbol.h"
#include "symbols/FunctionSymbol.h"

SymbolTableVisitor::SymbolTableVisitor(BaseScope* root_scope) {
  SetValue(root_scope);
}

void SymbolTableVisitor::Visit(NumberExpression* expression) {}

void SymbolTableVisitor::Visit(IdentExpression* expression) {
  tos_value_->GetElement(expression->ident_); 
}

void SymbolTableVisitor::Visit(NestedExpression* expression) {
  Accept(expression->expression_);
}

void SymbolTableVisitor::Visit(EqualExpression* expression) {
  Accept(expression->left_expression_);
  Accept(expression->right_expression_);
}

void SymbolTableVisitor::Visit(NotEqualExpression* expression) {
  Accept(expression->left_expression_);
  Accept(expression->right_expression_);
}

void SymbolTableVisitor::Visit(LessExpression* expression) {
  Accept(expression->left_expression_);
  Accept(expression->right_expression_);
}

void SymbolTableVisitor::Visit(LessEqualExpression* expression) {
  Accept(expression->left_expression_);
  Accept(expression->right_expression_);
}

void SymbolTableVisitor::Visit(GreaterExpression* expression) {
  Accept(expression->left_expression_);
  Accept(expression->right_expression_);
}

void SymbolTableVisitor::Visit(GreaterEqualExpression* expression) {
  Accept(expression->left_expression_);
  Accept(expression->right_expression_);
}

void SymbolTableVisitor::Visit(AddExpression* expression) {
  Accept(expression->left_expression_);
  Accept(expression->right_expression_);
}

void SymbolTableVisitor::Visit(SubExpression* expression) {
  Accept(expression->left_expression_);
  Accept(expression->right_expression_);
}

void SymbolTableVisitor::Visit(MulExpression* expression) {
  Accept(expression->left_expression_);
  Accept(expression->right_expression_);
}

void SymbolTableVisitor::Visit(DivExpression* expression) {
  Accept(expression->left_expression_);
  Accept(expression->right_expression_);
}

void SymbolTableVisitor::Visit(PrintStatement* statement) {
  Accept(statement->expression_);
}

void SymbolTableVisitor::Visit(VarDecl* var_decl) {
  tos_value_->AddElement(new VariableSymbol(var_decl->variable_, "int"));
}

void SymbolTableVisitor::Visit(IfElseStatement* statement) {
  Accept(statement->expression_);
  
  auto if_scope = new BaseScope(tos_value_);
  tos_value_->AddScope("if" + std::to_string(tos_value_->counter), if_scope);
  SetValue(if_scope);
  Accept(statement->if_statements_);
  SetValue(if_scope->GetParent());
  
  auto else_scope = new BaseScope(tos_value_);
  tos_value_->AddScope("else" + std::to_string(tos_value_->counter), else_scope);
  SetValue(else_scope);
  Accept(statement->else_statements_);
  SetValue(else_scope->GetParent());

  ++(tos_value_->counter);
}

void SymbolTableVisitor::Visit(Assignment* assignment) {
  Accept(assignment->expression_);
  tos_value_->GetElement(assignment->variable_);
}

void SymbolTableVisitor::Visit(StatementList* statement_list) {
  for (auto& statement: statement_list->statements_) {
    Accept(statement);
  }
}

void SymbolTableVisitor::Visit(Program* program) {
  tos_value_->AddElement(new FunctionSymbol("main"));
  auto main_scope = new BaseScope(tos_value_);
  tos_value_->AddScope("main", main_scope);
  SetValue(main_scope);
  Accept(program->statement_list_);
  SetValue(main_scope->GetParent());
  tos_value_->Reset();
}

