#pragma once

#include <include/driver.hh>
#include "TemplateVisitor.h"

class Interpreter: public TemplateVisitor<int> {
public:
    explicit Interpreter(Driver* driver);
    void Visit(NumberExpression* expression) override;
    void Visit(IdentExpression* expression) override;
    void Visit(NestedExpression* expression) override;
    void Visit(EqualExpression* expression) override;
    void Visit(NotEqualExpression* expression) override;
    void Visit(LessExpression* expression) override;
    void Visit(LessEqualExpression* expression) override;
    void Visit(GreaterExpression* expression) override;
    void Visit(GreaterEqualExpression* expression) override;
    void Visit(AddExpression* expression) override;
    void Visit(SubExpression* expression) override;
    void Visit(MulExpression* expression) override;
    void Visit(DivExpression* expression) override;
    void Visit(PrintStatement* statement) override;
    void Visit(VarDecl* var_decl) override;
    void Visit(IfElseStatement* statement) override;
    void Visit(Assignment* assignment) override;
    void Visit(StatementList* statement_list) override;
    void Visit(Program* program) override;
private:
    Driver* driver_;
};
