#include "PrintVisitor.h"
#include "elements.h"

#include <iostream>

PrintVisitor::PrintVisitor(const std::string& filename) : stream_(filename) {}
PrintVisitor::~PrintVisitor() {
    stream_.close();
}

void PrintVisitor::Visit(NumberExpression* expression) {
    PrintTabs();
    stream_ << "NumberExpression " << expression->value_ << std::endl;
}

void PrintVisitor::Visit(IdentExpression* expression) {
    PrintTabs();
    stream_ << "IdentExpression " << expression->ident_ << std::endl;
}

void PrintVisitor::Visit(NestedExpression* expression) {
    PrintTabs();
    stream_ << "NestedExpression" << std::endl;
    ++num_tabs_;
    expression->expression_->Accept(this);
    --num_tabs_;
}

void PrintVisitor::Visit(EqualExpression* expression) {
    PrintTabs();
    stream_ << "EqualExpression" << std::endl;
    ++num_tabs_;
    expression->left_expression_->Accept(this);
    expression->right_expression_->Accept(this);
    --num_tabs_;
}

void PrintVisitor::Visit(NotEqualExpression* expression) {
    PrintTabs();
    stream_ << "NotEqualExpression" << std::endl;
    ++num_tabs_;
    expression->left_expression_->Accept(this);
    expression->right_expression_->Accept(this);
    --num_tabs_;
}

void PrintVisitor::Visit(LessExpression* expression) {
    PrintTabs();
    stream_ << "LessExpression" << std::endl;
    ++num_tabs_;
    expression->left_expression_->Accept(this);
    expression->right_expression_->Accept(this);
    --num_tabs_;
}

void PrintVisitor::Visit(LessEqualExpression* expression) {
    PrintTabs();
    stream_ << "LessEqualExpression" << std::endl;
    ++num_tabs_;
    expression->left_expression_->Accept(this);
    expression->right_expression_->Accept(this);
    --num_tabs_;
}

void PrintVisitor::Visit(GreaterExpression* expression) {
    PrintTabs();
    stream_ << "GreaterExpression" << std::endl;
    ++num_tabs_;
    expression->left_expression_->Accept(this);
    expression->right_expression_->Accept(this);
    --num_tabs_;
}

void PrintVisitor::Visit(GreaterEqualExpression* expression) {
    PrintTabs();
    stream_ << "GreaterEqualExpression" << std::endl;
    ++num_tabs_;
    expression->left_expression_->Accept(this);
    expression->right_expression_->Accept(this);
    --num_tabs_;
}

void PrintVisitor::Visit(AddExpression* expression) {
    PrintTabs();
    stream_ << "AddExpression" << std::endl;
    ++num_tabs_;
    expression->left_expression_->Accept(this);
    expression->right_expression_->Accept(this);
    --num_tabs_;
}

void PrintVisitor::Visit(SubExpression* expression) {
    PrintTabs();
    stream_ << "SubExpression" << std::endl;
    ++num_tabs_;
    expression->left_expression_->Accept(this);
    expression->right_expression_->Accept(this);
    --num_tabs_;
}

void PrintVisitor::Visit(MulExpression* expression) {
    PrintTabs();
    stream_ << "MulExpression" << std::endl;
    ++num_tabs_;
    expression->left_expression_->Accept(this);
    expression->right_expression_->Accept(this);
    --num_tabs_;
}

void PrintVisitor::Visit(DivExpression* expression) {
    PrintTabs();
    stream_ << "DivExpression" << std::endl;
    ++num_tabs_;
    expression->left_expression_->Accept(this);
    expression->right_expression_->Accept(this);
    --num_tabs_;
}

void PrintVisitor::Visit(PrintStatement* statement) {
    PrintTabs();
    stream_ << "PrintStatement: " << std::endl;
    ++num_tabs_;
    statement->expression_->Accept(this);
    --num_tabs_;
}

void PrintVisitor::Visit(VarDecl* var_decl) {
    PrintTabs();
    stream_ << "VarDecl: " << var_decl->variable_ << std::endl;
}

void PrintVisitor::Visit(IfElseStatement* statement) {
    PrintTabs();
    stream_ << "IfElseStatement:" << std::endl;
    ++num_tabs_;
    statement->expression_->Accept(this);
    statement->if_statements_->Accept(this);
    statement->else_statements_->Accept(this);
    --num_tabs_;
}

void PrintVisitor::Visit(Assignment* assignment) {
    PrintTabs();
    stream_ << "Assignment: " << assignment->variable_ << std::endl;
    ++num_tabs_;
    assignment->expression_->Accept(this);
    --num_tabs_;
}

void PrintVisitor::Visit(StatementList* statement_list) {
    PrintTabs();
    stream_ << "StatementList: " << std::endl;
    ++num_tabs_;
    for (Statement* statement: statement_list->statements_) {
        statement->Accept(this);
    }
    --num_tabs_;
}

void PrintVisitor::Visit(Program* program) {
    PrintTabs();
    stream_ << "Program:" << std::endl;
    ++num_tabs_;
    program->statement_list_->Accept(this);
    --num_tabs_;
}


void PrintVisitor::PrintTabs() {
    for (int i = 0; i < num_tabs_; ++i) {
        stream_ << "\t";
    }
}
