#pragma once

#include "../grammar/expressions/NumberExpression.h"
#include "../grammar/expressions/IdentExpression.h"
#include "../grammar/expressions/NestedExpression.h"
#include "../grammar/expressions/EqualExpression.h"
#include "../grammar/expressions/NotEqualExpression.h"
#include "../grammar/expressions/LessExpression.h"
#include "../grammar/expressions/LessEqualExpression.h"
#include "../grammar/expressions/GreaterExpression.h"
#include "../grammar/expressions/GreaterEqualExpression.h"
#include "../grammar/expressions/AddExpression.h"
#include "../grammar/expressions/SubExpression.h"
#include "../grammar/expressions/MulExpression.h"
#include "../grammar/expressions/DivExpression.h"


#include "../grammar/statements/Statement.h"
#include "../grammar/statements/StatementList.h"
#include "../grammar/statements/Assignment.h"
#include "../grammar/statements/VarDecl.h"
#include "../grammar/statements/PrintStatement.h"
#include "../grammar/statements/IfElseStatement.h"

#include "../grammar/Program.h"
