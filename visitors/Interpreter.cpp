#include <iostream>
#include "Interpreter.h"
#include "elements.h"

Interpreter::Interpreter(Driver* driver) : driver_(driver) {}

void Interpreter::Visit(NumberExpression* expression) {
  SetValue(expression->value_);
}

void Interpreter::Visit(IdentExpression* expression) {
  // Checking if the variable was declared before usage
  assert(driver_->variables.find(expression->ident_) != driver_->variables.end());
  SetValue(driver_->variables[expression->ident_]);
}

void Interpreter::Visit(NestedExpression* expression) {
  SetValue(Accept(expression->expression_));
}

void Interpreter::Visit(EqualExpression* expression) {
  SetValue(Accept(expression->left_expression_) == Accept(expression->right_expression_));
}

void Interpreter::Visit(NotEqualExpression* expression) {
  SetValue(Accept(expression->left_expression_) != Accept(expression->right_expression_));
}

void Interpreter::Visit(LessExpression* expression) {
  SetValue(Accept(expression->left_expression_) < Accept(expression->right_expression_));
}

void Interpreter::Visit(LessEqualExpression* expression) {
  SetValue(Accept(expression->left_expression_) <= Accept(expression->right_expression_));
}

void Interpreter::Visit(GreaterExpression* expression) {
  SetValue(Accept(expression->left_expression_) > Accept(expression->right_expression_));
}

void Interpreter::Visit(GreaterEqualExpression* expression) {
  SetValue(Accept(expression->left_expression_) >= Accept(expression->right_expression_));
}

void Interpreter::Visit(AddExpression* expression) {
  SetValue(Accept(expression->left_expression_) + Accept(expression->right_expression_));
}

void Interpreter::Visit(SubExpression* expression) {
  SetValue(Accept(expression->left_expression_) - Accept(expression->right_expression_));
}

void Interpreter::Visit(MulExpression* expression) {
  SetValue(Accept(expression->left_expression_) * Accept(expression->right_expression_));
}

void Interpreter::Visit(DivExpression* expression) {
  SetValue(Accept(expression->left_expression_) / Accept(expression->right_expression_));
}

void Interpreter::Visit(PrintStatement* statement) {
  std::cout << "PRINT VALUE" << std::endl;
  std::cout << Accept(statement->expression_) << std::endl;
}

void Interpreter::Visit(VarDecl* var_decl) {
  driver_->variables[var_decl->variable_];
}

void Interpreter::Visit(IfElseStatement* statement) {
  if (Accept(statement->expression_)) {
    statement->if_statements_->Accept(this);
  } else {
    statement->else_statements_->Accept(this);
  }
}

void Interpreter::Visit(Assignment* assignment) {
  // Checking if the variable was declared before assignment
  assert(driver_->variables.find(assignment->variable_) != driver_->variables.end());
  driver_->variables[assignment->variable_] = Accept(assignment->expression_);
}

void Interpreter::Visit(StatementList* statement_list) {
  for (auto* statement : statement_list->statements_) {
    statement->Accept(this);
  }
}

void Interpreter::Visit(Program* program) {
  program->statement_list_->Accept(this);
}
