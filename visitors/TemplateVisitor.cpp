#include "TemplateVisitor.h"
#include "elements.h"
#include "scopes/BaseScope.h"
#include <llvm/IR/Value.h>

template <typename T>
T TemplateVisitor<T>::Accept(BaseElement* element) {
    element->Accept(this);
    return tos_value_;
}

template int TemplateVisitor<int>::Accept(BaseElement* element);

template llvm::Value* TemplateVisitor<llvm::Value*>::Accept(BaseElement* element);

template BaseScope* TemplateVisitor<BaseScope*>::Accept(BaseElement* element);

template <typename T>
void TemplateVisitor<T>::SetValue(T value) {
    tos_value_ = value;
}

template void TemplateVisitor<int>::SetValue(int value);

template void TemplateVisitor<llvm::Value*>::SetValue(llvm::Value* value);

template void TemplateVisitor<BaseScope*>::SetValue(BaseScope* value);
