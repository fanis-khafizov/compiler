#pragma once

#include <string>
#include <unordered_map>
#include <vector>
#include "symbols/BaseSymbol.h"

class BaseScope {
public:
  BaseScope() = default;
  BaseScope(BaseScope* parent);
  ~BaseScope();
  BaseSymbol* GetElement(std::string name);
  void AddElement(BaseSymbol* symbol);
  BaseScope* GetScope(std::string name);
  void AddScope(std::string name, BaseScope* scope);
  BaseScope* GetParent();
  void Reset();
  void Print();
private:
  std::unordered_map<std::string, BaseScope*> children;
  std::unordered_map<std::string, BaseSymbol*> elements;
  BaseScope* parent = nullptr;
public:
  int counter = 0;
};
