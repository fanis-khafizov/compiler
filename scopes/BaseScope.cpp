#include "BaseScope.h"
#include <iostream>
#include <stdexcept>

BaseScope::BaseScope(BaseScope* parent) : parent(parent) {}

BaseSymbol* BaseScope::GetElement(std::string name) {
  if (elements.find(name) != elements.end()) {
    return elements[name];
  } else {
    if (parent == nullptr) {
      throw std::runtime_error("Element has not been declared!");
    }
    return parent->GetElement(name);
  }
}

void BaseScope::AddElement(BaseSymbol* symbol) {
  if (elements.find(symbol->name_) != elements.end()) {
    throw std::runtime_error("Element has already been declared!");
  }
  elements[symbol->name_] = symbol;
}

BaseScope* BaseScope::GetScope(std::string name) {
  if (children.find(name) == children.end()) {
    throw std::runtime_error("Scope is not found!");
  }
  return children[name];
}

void BaseScope::AddScope(std::string name, BaseScope* scope) {
  if (children.find(name) != children.end()) {
    throw std::runtime_error("Scope has already been declared!");
  }
  children[name] = scope;
}

BaseScope* BaseScope::GetParent() {
  return parent;
}

void BaseScope::Reset() {
  counter = 0;
  for (auto scope: children) {
    scope.second->Reset();
  }
}

void BaseScope::Print() {
  for (auto element: elements) {
    std::cout << element.first << std::endl;
  }
  for (auto scope: children) {
    std::cout << scope.first << '(' << std::endl;
    scope.second->Print();
    std::cout << ')' << std::endl;
  }
}

BaseScope::~BaseScope() {
  for (auto iter: elements) {
    delete iter.second;
  }
  for (auto iter: children) {
    delete iter.second;
  }
}

